# Readme file

## Install components

This works for windows.

Firstly we need to install minikube, docker and helm with chocolatey.

```sh
choco install minikube kubernetes-helm docker -y
```

## Start the Environment

Secondly we need to start minikube. Hyper-V should be accessible.

```sh
minikube start
minikube addons enable ingress
minikube addons enable metrics-server
minikube dashboard
```

## Deploy Services to Kubernetes

```sh
# Lint files
helm lint .
# Look at constructed templates
helm template .
# Deploy services
helm install firstservice .
# Activate endpoint of ingress
minikube service adminer-local-service -n prod
```
